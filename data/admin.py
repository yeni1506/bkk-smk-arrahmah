from django.contrib import admin
from .models import Data_alumni
from .models import Data_perusahaan
from .models import Data_loker

# Register your models here.
@admin.register(Data_alumni)
class DataAlumniAdmin(admin.ModelAdmin):
	list_display = ['nama_siswa','nis','jurusan','alamat','ttl','status']
	list_filter = ['nama_siswa','nis','jurusan','alamat','ttl','user']
	search_fields = ['nama_siswa','nis']


# @admin.register(Data_perusahaan)
# class DataPerusahaaniAdmin(admin.ModelAdmin):
# 	list_display = ['nama_perusahaan','no_telepon','email']
# 	list_filter = ['nama_perusahaan','no_telepon','email']
# 	search_fields = ['nama_perusahaan','no_telepon','email']

@admin.register(Data_loker)
class DataLokeriAdmin(admin.ModelAdmin):
	list_display = ['nama_perusahaan','sektor_usaha','alamat','telepon','email','posisi_dibutuhkan','jumlah_kebutuhan','jenkel','batas_waktu','deskripsi']
	list_filter = ['nama_perusahaan','sektor_usaha','alamat','telepon','email','posisi_dibutuhkan','jumlah_kebutuhan','jenkel','batas_waktu','deskripsi']
	search_fields = ['nama_perusahaan','sektor_usaha','posisi_dibutuhkan']


