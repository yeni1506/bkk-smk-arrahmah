"""my_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import index, AlumniCreateView, AlumniEditView, AlumniDeleteView, AlumniToPdf, LokerView, LokerCreateView, LokerEditView, LokerDeleteView, LokerToPdf

urlpatterns = [
    path('',index,name='home_page'),
    path('alumni/add', AlumniCreateView.as_view(),name='alumni_add'),
    path('alumni/edit/<int:pk>',AlumniEditView.as_view(),name='alumni_edit'),
    path('alumni/delete/<int:pk>',AlumniDeleteView.as_view(),name='alumni_delete'),
    path('alumni/print_pdf', AlumniToPdf.as_view(),name='alumni_to_pdf'),
    path('loker/data_loker',LokerView,name='data_loker'),
    path('loker/add', LokerCreateView.as_view(),name='loker_add'),
    path('loker/edit/<int:pk>',LokerEditView.as_view(),name='loker_edit'),
    path('loker/delete/<int:pk>',LokerDeleteView.as_view(),name='loker_delete'),
    path('loker/print_pdf', LokerToPdf.as_view(),name='loker_to_pdf'),

]


