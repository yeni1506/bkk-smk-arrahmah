from .models import Data_alumni
from .models import Data_loker
from django.shortcuts import render
from django.urls import reverse_lazy
from .utils import Render
from django.views.generic import CreateView, UpdateView, DeleteView, View

# Create your views here.

var = {
	'judul':'Toko Pertukangan Aneka Kerja',
	'info':'''Kami menyediakan segala peralatan dan pertukangan Anda, baik untuk hobi mauntuk untuk profesi and ''',
	'oleh':'owner'
}

def index(self):
	var['data'] = Data_alumni.objects.values('id','nama_siswa','nis','jurusan','status','alamat','ttl','tgl_input','user').order_by('nama_siswa')
	return render(self,'data/index.html',context=var)


class AlumniCreateView(CreateView):
	model = Data_alumni
	fields = '__all__'
	template_name = 'data/alumni_add.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AlumniEditView(UpdateView):
	model = Data_alumni
	fields = ['nama_siswa','nis','jurusan','status','alamat','ttl']
	template_name = 'data/alumni_edit.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AlumniDeleteView(DeleteView):
	model = Data_alumni
	template_name = 'data/alumni_delete.html'
	success_url = reverse_lazy('home_page')

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AlumniToPdf(View):
	def get(self,request):
		var = {
		'data' : Data_alumni.objects.values('nama_siswa','nis','jurusan','alamat','ttl'),'request':request
		}
		return Render.to_pdf(self,'data/alumni_to_pdf.html',var)

def LokerView(self):
	var['data'] = Data_loker.objects.values('id','nama_perusahaan','sektor_usaha','alamat','telepon','email','posisi_dibutuhkan','jumlah_kebutuhan','jenkel','batas_waktu','deskripsi','tgl_input','user').order_by('nama_perusahaan')
	return render(self,'data/loker.html',context=var)


class LokerCreateView(CreateView):
	model = Data_loker
	fields = '__all__'
	template_name = 'data/loker_add.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class LokerEditView(UpdateView):
	model = Data_loker
	fields = ['nama_perusahaan','sektor_usaha','alamat','telepon','email','posisi_dibutuhkan','jumlah_kebutuhan','jenkel','batas_waktu','deskripsi']
	template_name = 'data/loker_edit.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class LokerDeleteView(DeleteView):
	model = Data_loker
	template_name = 'data/loker_delete.html'
	success_url = reverse_lazy('data_loker')

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class LokerToPdf(View):
	def get(self,request):
		var = {
		'data' : Data_loker.objects.values('nama_perusahaan','sektor_usaha','alamat','telepon','email','posisi_dibutuhkan','jumlah_kebutuhan','jenkel','batas_waktu','deskripsi'),'request':request
		}
		return Render.to_pdf(self,'data/loker_to_pdf.html',var)