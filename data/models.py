from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone

# Create your models here.

class Data_alumni(models.Model):
	JURUSAN_CHOICES = (
		('TKJ','Teknik Komputer Jaringan'),
		('TKR','Teknik Kendaraan Ringan'),
		('TSM','Teknik Sepeda Motor'),
	);
	STATUS_CHOICES = (
		('Bekerja','Bekerja'),
		('Belum Bekerja','Belum Bekerja'),
		('Kuliah','Kuliah'),
	)

	nama_siswa = models.CharField('Nama Lengkap',max_length=50,null=False)
	nis = models.IntegerField()
	jurusan = models.CharField(max_length=3,choices=JURUSAN_CHOICES)
	status = models.CharField(max_length=19,choices=STATUS_CHOICES)
	alamat = models.TextField()
	ttl = models.CharField('TTL', max_length=50,null=False)
	tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']

	def __str__(self):
		return self.nama_siswa

	def get_absolute_url(self):
		return reverse('home_page')


class Data_perusahaan(models.Model):
	
	nama_perusahaan = models.CharField('Nama Perusahaan',max_length=50,null=False)
	sektor_usaha = models.CharField('Sektor Usaha',max_length=50,null=False)
	no_telepon = models.IntegerField()
	alamat = models.TextField()
	email = models.CharField('Email',max_length=50,null=False)
	deskripsi = models.TextField('Deskripsi')
	tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']

	def __str__(self):
		return self.nama_perusahaan

	def get_absolute_url(self):
		return reverse('home_page')


class Data_loker(models.Model):
	JENKEL_CHOICES = (
		('Laki-Laki','Laki-Laki'),
		('Perempuan','Perempuan'),
		('Laki-Laki/Perempuan','Laki-Laki/Perempuan'),
	)

	nama_perusahaan = models.CharField('Nama Perusahaan',max_length=50,null=False)
	sektor_usaha = models.CharField('Sektor Usaha',max_length=50,null=False)
	telepon = models.IntegerField()
	alamat = models.TextField()
	email = models.CharField('Email',max_length=50,null=False)
	posisi_dibutuhkan = models.CharField('Posisi yang Dibutuhkan',max_length=50,null=False)
	jumlah_kebutuhan = models.CharField('Jumlah Kebutuhan',max_length=50,null=False)
	jenkel = models.CharField(max_length=19,choices=JENKEL_CHOICES)
	batas_waktu = models.CharField('Batas Waktu',max_length=50,null=False)
	deskripsi = models.TextField()
	tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']

	def __str__(self):
		return self.nama_perusahaan

	def get_absolute_url(self):
		return reverse('data_loker')

